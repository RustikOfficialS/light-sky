from django.shortcuts import render, redirect
from .forms import CreateUserForm
from django.contrib import messages
from .models import Post
from .models import UserInfo
from django.contrib.auth.models import User
from .forms import PostForm
from .forms import UserInfoForm

from django.contrib.auth import authenticate, login, logout

from django.contrib.auth.decorators import login_required


def register_page(request):
    if request.user.is_authenticated:
        return redirect('posts')
    else:
        form = CreateUserForm()
        if request.method == "POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('login')

        context = {'form':form}
        return render(request, 'register.html', context)


def login_page(request):
    if request.user.is_authenticated:
        return redirect('posts')
    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('posts')
            else:
                messages.info(request, 'Неверное имя пользователя или пароль')

        context={}
        return render(request, 'login.html', context)


def logoutUser(request):
    logout(request)
    return redirect('login')


@login_required(login_url='login')
def main_page(request):
    posts = Post.objects.order_by("-id")

    context = {
        'title':'Главная страница сайта',
        'posts':posts,
    }
    return render(request, 'main.html', context)


@login_required(login_url='login')
def create_post(request):
    error = ''
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save()
            post.image = request.FILES.get('image')
            post.user = request.user
            post.save()
            return redirect('posts')
    form = PostForm()
    context = {
        'form' : form,
        'error' : error,
    }
    return render(request, 'create_post.html', context)


@login_required(login_url='login')
def user_profile(request, user_id=None):
    user = User.objects.filter(id=user_id).first()

    if not user:
        return redirect('posts')

    post = Post.objects.filter(user=user).values()

    return render(request, 'user_profile.html', locals())


@login_required(login_url='login')
def my_profile(request):

    user = request.user

    post = Post.objects.filter(user=user).values()

    about = UserInfo.objects.filter(user=user).values()

    return render(request, 'my_profile.html', locals())


@login_required(login_url='login')
def change_profile(request):
    error = ''
    if request.method == 'POST':
        form = UserInfoForm(request.POST, request.FILES)
        if form.is_valid():
            info = form.save()
            info.image = request.FILES.get('image')
            info.user = request.user
            info.save()
            return redirect('my_profile')
    form = UserInfoForm()
    context = {
        'form' : form,
        'error' : error,
    }
    return render(request, 'change_profile.html', context)