# Generated by Django 4.1 on 2022-08-17 19:31

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('base', '0004_post_created_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='post',
            field=models.TextField(max_length=500, verbose_name='Описание'),
        ),
        migrations.CreateModel(
            name='UserInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('about', models.CharField(max_length=255, verbose_name='Описание')),
                ('avatar', models.ImageField(blank=True, null=True, upload_to='images/', verbose_name='Аватарка')),
                ('banner', models.ImageField(blank=True, null=True, upload_to='images/', verbose_name='Баннер')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Пользователь')),
            ],
            options={
                'verbose_name': 'Информация',
                'verbose_name_plural': 'Информация',
            },
        ),
    ]
