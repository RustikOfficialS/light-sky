from django.contrib.auth.models import User
from django.db import models

class Post(models.Model):
    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'

    title = models.CharField('Название', max_length=30, blank=False, null=False)
    post = models.TextField('Описание', max_length=500, blank=False, null=False)
    image = models.ImageField('Изображение', upload_to='images/', blank=True, null=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', blank=True, null=True, on_delete=models.CASCADE)
    created_at = models.DateTimeField('Время создания', auto_now=True)
    def __str__(self):
        return f'{self.post}'


class UserInfo(models.Model):
    class Meta:
        verbose_name = 'Информация'
        verbose_name_plural = 'Информация'

    about = models.CharField('Описание', max_length=255, blank=False, null=False)
    avatar = models.ImageField('Аватарка', upload_to='images/', blank=True, null=True)
    banner = models.ImageField('Баннер', upload_to='images/', blank=True, null=True)
    user = models.ForeignKey(User, verbose_name='Пользователь', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.about}'