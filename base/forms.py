from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Post
from .models import UserInfo
from django.forms import ModelForm, TextInput, Textarea
from django import forms


class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

    email = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Email..'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Username..'}))
    password1 = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Enter password..'}))
    password2 = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Re-enter password..'}))


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'post', 'image']
        widgets = {
            'title': TextInput(attrs={
                'class':'title',
                'placeholder':'Название поста'
            }),
            'post': Textarea(attrs={
                'class':'post',
                'placeholder':'Что у вас нового?'
            }),
        }


class UserInfoForm(forms.ModelForm):
    class Meta:
        model = UserInfo
        fields = ['about', 'avatar', 'banner']
        widgets = {
            'about': forms.Textarea(attrs={
                'class':'about',
                'placeholder':'Описание'
            })
        }