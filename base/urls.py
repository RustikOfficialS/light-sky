from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.main_page, name="posts"),
    path('register/', views.register_page, name="register"),
    path('login/', views.login_page, name="login"),
    path('logout/', views.logoutUser, name="logout"),
    path('create_post/', views.create_post, name='create_post'),
    path('user_profile/', views.user_profile, name="user_profile"),
    path('user_profile/<int:user_id>/', views.user_profile, name='user_profile'),
    path('my_profile/', views.my_profile, name="my_profile"),
    path('change_profile/', views.change_profile, name="change_file"),
] + static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)